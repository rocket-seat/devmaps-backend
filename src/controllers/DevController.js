const axios = require("axios");
const Dev = require("../models/Dev");
const parseStringAsArray = require("../utils/parseStringAsArray");
const { findConnections, sendMessage } = require("../websocket");

module.exports = {
  async getAll(req, res) {
    const devs = await Dev.find();
    return res.json(devs);
  },

  async getByID(req, res) {
    const dev = await Dev.findById(req.params.id);
    return res.json(dev);
  },

  async incluir(req, res) {
    const { github_username, techs, latitude, longitude } = req.body;

    let dev = await Dev.findOne({ github_username });

    if (!dev) {
      const response = await axios.get("https://api.github.com/users/" + github_username);

      const { name = login, avatar_url, bio } = response.data;

      const techsArray = parseStringAsArray(techs);

      const location = {
        type: "Point",
        coordinates: [longitude, latitude]
      };

      dev = await Dev.create({
        github_username,
        name,
        avatar_url,
        bio,
        techs: techsArray,
        location
      });

      // Filtrar as conexoes que estao no maximo 10km
      const sendMessageTo = findConnections({ latitude, longitude }, techsArray);
      sendMessage(sendMessageTo, "new-dev", dev);
    }

    return res.json(dev);
  },

  async editar(req, res) {
    const dev = await Dev.findByIdAndUpdate(req.params.id, req.body, { new: true });
    return res.json(dev);
  },

  async excluir(req, res) {
    const dev = await Dev.findById(req.params.id);

    // Filtrar as conexoes que estao no maximo 10km
    const sendDeleteTo = findConnections({ longitude: dev.location.coordinates[0], latitude: dev.location.coordinates[1] }, dev.techs);
    sendMessage(sendDeleteTo, "del-dev", dev);

    // Remove
    await Dev.findByIdAndDelete(req.params.id);
    return res.send();
  }
};
