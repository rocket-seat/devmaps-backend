const socketio = require("socket.io");
const parseStringAsArray = require("./utils/parseStringAsArray");
const calculateDistance = require("./utils/calculateDistance");

let io;
const connections = [];

exports.setupWebsocket = server => {
  io = socketio(server);

  io.on("connection", socket => {
    const { latitude, longitude, techs, isPC } = socket.handshake.query;

    connections.push({
      id: socket.id,
      isPC: Boolean(isPC),
      coordinates: {
        latitude: Number(latitude),
        longitude: Number(longitude)
      },
      techs: parseStringAsArray(techs)
    });
  });
};

exports.findConnections = (coordinates, techs) => {
  return connections.filter(connection => {
    let res;

    if (!connection.isPC) {
      res = calculateDistance(coordinates, connection.coordinates) < 10 && connection.techs.some(t => techs.includes(t));
    } else {
      res = calculateDistance(coordinates, connection.coordinates) < 10;
    }

    return res;
  });
};

exports.sendMessage = (to, message, data) => {
  to.forEach(connection => {
    io.to(connection.id).emit(message, data);
  });
};
