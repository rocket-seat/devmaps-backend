const { Router } = require("express");
const DevController = require("./controllers/DevController");
const SearchController = require("./controllers/SearchController");

const routes = Router();

routes.get("/search", SearchController.index);

routes.get("/devs", DevController.getAll);
routes.get("/devs/:id", DevController.getByID);
routes.put("/devs/:id", DevController.editar);
routes.delete("/devs/:id", DevController.excluir);
routes.post("/devs", DevController.incluir);

module.exports = routes;
